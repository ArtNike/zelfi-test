@extends('layouts.app')


@section('content')

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <style>
        h4 {
            overflow: auto;
            word-wrap: break-word;
        }
        p {
            overflow: auto;
            word-wrap: break-word;

        }

        .row {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display:         flex;
            flex-wrap: wrap;
        }
        .row > [class*='col-'] {
            display: flex;
            flex-direction: column;
        }

        .desc-container {
            height: 400px;
            overflow-y: scroll;
        }
    </style>

    <div class="container">
        <input id="search" type="text" class="form-control" placeholder="Поиск мероприятия">
        <div class = "col-md-12">
            <h2>Мероприятия</h2>
            <div id="main-container" class = "row">

                @foreach($events as $event)

                    <div class = "col-sm-4">
                        <div class = "panel">
                            <h4 class="text-center">{{$event->name}}</h4>
                            <img src = "images/{{$event->image}}" class = "img-responsive center-block" width="250" height="250">
                            <p class="text-center">{{$event->description}}</p>
                            <div class="row">
                                <button type="button" class="btn btn-primary center-block col-md-3" onclick="window.location.href='/event/{{$event->id}}'"  style="margin-left: 50px">Подробнее</button>
                                @if(Auth::guest())
                                <button type="button" class="btn btn-primary center-block col-md-5" onclick="window.location.href='/login'" style="margin-left: 30px">Зарегистрироваться</button>
                                    @else
                                    <button id="{{$event->id}}" type="button" class="btn btn-primary center-block col-md-5"  style="margin-left: 30px">Зарегистрироваться</button>
                                    @endif
                            </div>

                        </div>
                    </div>

                    @endforeach

             </div>
        </div>
    </div>

    <script type="text/javascript">
        $( document ).ready(function() {
            $('#search').keyup(function () {
                    $.post( "/search", { name: $('#search').val(),  "_token": "{{ csrf_token() }}",})
                        .done(function( data ) {
                                console.log(data);
                                $('#main-container').empty();


                                $.each(data['data'], function (key, value) {
                                    $('#main-container').append('<div class = "col-sm-4">' +
                                        '<div class = "panel">' +
                                        ' <h4 class="text-center">'+value['name']+'</h4>' +
                                        '<img src = "images/'+value['image']+'" class = "img-responsive center-block" width="250" height="250">' +
                                        '<p class="text-center">'+value['description']+'</p>' +
                                        '<div class="row">' +
                                        '<button type="button" class="btn btn-primary center-block col-md-3" onclick="window.location.href=\'/event/'+value['id']+'\'"  style="margin-left: 50px">Подробнее</button>' +
                                        '<button id="'+value['id']+'" type="button" class="btn btn-primary center-block col-md-5"  style="margin-left: 30px">Зарегистрироваться</button>');
                                    
                                });
                            addListeners();
                        });

            });

            function addListeners() {
                $('.col-md-5').click(function () {
                    $.post( "/subscribe", { id: $(this).attr('id'),  "_token": "{{ csrf_token() }}",})
                        .done(function (data) {
                            alert('Вы зарегистрировались на мероприятие');
                        })
                })
            };
            addListeners();
        });
    </script>
@endsection