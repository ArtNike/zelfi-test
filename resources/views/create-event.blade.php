@extends('layouts.app')

@section('content')

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}">



    <div class="container">
        <div class="col-md-8"><h3>Зарегистрировать мероприятие</h3></div>
        <div class = "panel col-md-8">
            <form action="/create" method="POST">
                {{ csrf_field() }}

                <div class="form-group">
                    <label class="control-label " for="name">Название мероприятия*</label>
                    <input class="form-control" id="name" name="name" type="text"/>
                </div>


                <div class="form-group">
                    <label class="control-label " for="message">Описание*</label>
                    <textarea class="form-control" cols="40" id="message" name="message" rows="10"></textarea>
                </div>

                <div class="fileinput fileinput-new" data-provides="fileinput">
                    <span class="btn btn-default btn-file"><span>Choose file</span><input type="file" /></span>
                </div>

                <div class="form-group">
                    <button class="btn btn-primary" id="submit"  name="submit" type="submit">Зарегистрировать</button>
                </div>

            </form>
        </div>
    </div>

    <script type="text/javascript">
        $( document ).ready(function() {
            $('#submit').prop('disabled', true);
            $('input, textarea').keyup(function () {
                if(($('#name').val().length>4)&&($('#message').val().length>4)){
                    $('#submit').prop('disabled', false);
                }
                else {
                    $('#submit').prop('disabled', true);
                }
            })
        });

    </script>
@endsection