@extends('layouts.app')

@section('content')

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}">

<style>

    h4 {
        overflow: auto;
        word-wrap: break-word;
    }

</style>

    <div class="container">
        <div class="row">
            <div class="col-md-3"><h3>Мероприятие</h3></div>


        </div>

        <div class = "panel col-md-7">
            <div class="container">
                <div class="col-md-7">
                    <div class="row">
                        <div class="col-md-8">
                            <img src = "../images/{{$event->image}}" class = "img-responsive" width="250" height="250">
                            @if(Auth::guest())
                            <button type="button" class="btn btn-primary center-block col-md-5"onclick="window.location.href='/login'" style="margin-left: 50px">Зарегистрироваться</button>
                                @else
                                <button type="button" id="{{$event->id}}" class="btn btn-primary center-block col-md-5" style="margin-left: 50px">Зарегистрироваться</button>
                                @endif
                        </div>
                    </div>


                    <h3>Название:</h3>
                    <h4>{{$event->name}}</h4>
                    <h3>Описание</h3>
                    <h4>{{$event->description}}</h4>
                </div>

                </div>
    <p id="hiddenid" hidden>{{$event->id}}</p>

        </div>
    </div>

    <script type="text/javascript">
        $( document ).ready(function() {
            $('#subbutton').click(function () {
                $.post( "/subscribe", { id: $('#hiddenid').text(),  "_token": "{{ csrf_token() }}"})
                    .done(function (data) {
                        if(data['data']=='ok'){
                            $('#subbutton').remove();
                            alert('Вы зарегистрировались на мероприятие');
                        }
                    })
            })
        });
    </script>
@endsection