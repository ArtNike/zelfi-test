<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();
Route::get('/', 'EventController@mainPage');
Route::get('create', 'EventController@create');
Route::post('create', 'EventController@create');
Route::get('profile', 'ProfileController@index');
Route::post('subscribe', 'EventController@subscribe');
Route::get('event/{id}', 'EventController@index');
Route::post('search', 'EventController@search');

