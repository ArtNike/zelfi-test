<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class EventController extends Controller
{
    public function index($id){

        $event = DB::table('events')->where('id', '=', $id)->first();
        return view('event', ['event' => $event]);
    }

    public function mainPage(){
        $events = DB::table('events')->get();
        return view('main', ['events' => $events]);
    }

    public function create(Request $request){
        if($request->isMethod('get')){ //если get то просто возвращаем вьюху
            return view('create-event');
        }
        else{ //иначе post
            if(($request->input('name')!='')&&($request->input('message')!='')){
                DB::table('events')->insert([
                    'name' => $request->input('name'),
                    'description' => $request->input('message'),
                    'image' => 'photo.jpg',
                    'tags_id' => '',
                ]);
            }
         return redirect('/');
        }

    }

    public function search(Request $request){

        if($request->input('name')!=''){
            $events = DB::table('events')->where('name', 'like', '%'. $request->input('name') .'%')->get();
            if(sizeof($events)==0){
                $events = DB::table('events')->limit(15)->get();
            }
        }
        else{
            $events = DB::table('events')->limit(15)->get();
        }
        return response()->json(['data' => $events]);

}

    public function subscribe(Request $request){
        $user_events = DB::table('users')->where('id', '=', Auth::user()->id)->select('subscribed_events')->first();
        if($user_events->subscribed_events!=''){
            $user_events_array = explode(',', $user_events->subscribed_events);
            $strcmp=1;
            foreach ($user_events_array as $event){
                $strcmp = strcmp($event, $request->input('id'));
                if($strcmp==0){
                    break;
                }
                if($strcmp!=0){
                    DB::table('users')
                        ->where('id', Auth::user()->id)
                        ->update(['subscribed_events' =>$user_events->subscribed_events . ',' . $request->input('id')]);
                }
            }
        }
        else{

            DB::table('users')
                ->where('id', Auth::user()->id)
                ->update(['subscribed_events' => $request->input('id')]);

        }


        return response()->json(['data' => 'ok']);

    }


}
