<?php

use Illuminate\Database\Seeder;

class tags_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tags')->insert([
            'value' => 'it',
        ]);

        DB::table('tags')->insert([
            'value' => 'tv',
        ]);

        DB::table('tags')->insert([
            'value' => 'cats',
        ]);

        DB::table('tags')->insert([
            'value' => 'dogs',
        ]);

        DB::table('tags')->insert([
            'value' => 'comics',
        ]);

        DB::table('tags')->insert([
            'value' => 'rap',
        ]);

        DB::table('tags')->insert([
            'value' => 'rock',
        ]);

        DB::table('tags')->insert([
            'value' => 'sport',
        ]);

        DB::table('tags')->insert([
            'value' => 'business',
        ]);

        DB::table('tags')->insert([
            'value' => 'joga',
        ]);

        DB::table('tags')->insert([
            'value' => 'cinema',
        ]);
    }
}
