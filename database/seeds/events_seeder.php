<?php

use Illuminate\Database\Seeder;

class events_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('events')->insert([
            'name' => 'Круг любителей собак',
            'description' => 'Тут немного текста',
            'image' => 'photo.jpg',
            'tags_id' => '4',
        ]);

        DB::table('events')->insert([
            'name' => 'Круг любителей кошек',
            'description' => 'Тут еще текст',
            'image' => 'photo.jpg',
            'tags_id' => '3',
        ]);
    }
}
